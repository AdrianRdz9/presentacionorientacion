Administración 7.41\
\
Características del estudiante\
*Debe poseer conocimientos de derecho, economía e historia, geografías, matemáticas y cultura general.
*Necesitan contar con iniciativa, perseverancia, ética, creatividad y capacidad de análisis y síntesis.
*Deben de ser hábiles para poder comunicarse fácilmente de manera verbal y escrita, y poder crear relaciones interpersonales
*Saber trabajar en equipo$


Administración agropecuaria 7.32\
\
Características del estudiante \
*Deben poseer habilidades para la búsqueda, análisis y síntesis de información *Conocimientos básicos de matemáticas y estadísticas
*Capacidad para comunicarse de manera oral y escrita y poder crear relaciones interpersonales
*Saber integrar y dirigir grupos de trabajo
*Interés de elevar el nivel de los productores del campo$

Antropología\
\
Características del estudiante \
*Comprender problemas de aspecto social y cultural
*Poder relacionarse en contextos multiculturales
*Interés por explorar fenómenos culturales y sociales
*Disciplina en el estudio y gusto por la cultura
*Tener respeto por la diversidad cultural
*Tener conocimientos generales de geografía e historia de México y el mundo$


Ciencias de la comunicación\
\
Características del estudiante \
*Conocimiento óptimo de la gramática
*Capacidad de redacción, uso del lenguaje verbal y escrito
*Facilidad de trato personal$

Ciencias políticas y administración publica 7.60\
\
Características del estudiante\
*Solida información en las disciplinas económicas
*Capacidad de abstracción y análisis
*Adquirir el hábito cotidiano de la lectura
*Conocimientos básicos en matemáticas, economía, sociología, historia, filosofía y redacción
*Tener interés en asuntos del gobierno$

Comunicación\
\
Características del estudiante\
*Interés por el estudio y la búsqueda de soluciones a problemas
*Conocimiento social y humanístico
*Conocimientos de historia
*Habilidad para la lectura y comprensión lectora
*Capacidad crítica, reflexiva, de análisis
*Capacidad de trabajo autónomo y grupal
*Compromiso con la sociedad
*Manejo de expresión oral y escrita$

Comunicación y periodismo 7.53\
\
Características del estudiante \
*Habilidad para comparar y manejar los sucesos
*Capacidad de análisis
*Conocimiento óptimo de la gramática y habilidad para la lectura
*Facilidad de trato personal y capacidad de trabajar de manera grupal
*Imaginación, creatividad e inventiva$

Contaduría 7.13\
\
Características del estudiante\
*Conocimientos de derecho, historia, matemáticas y metodología de la investigación
*Capacidad de análisis y de observación
*Tener interés por el trabajo con números
*Capacidad de trabajar en equipo y tener ética personal$

Derecho 7.20\
\
Características del estudiante\
*Interés por los problemas sociales en el ámbito económico, político, económico y cultural; de orden nacional e internacional
*Manejo adecuado de la expresión oral y escrita
*Gusto por el debate
*Razonamiento lógico y pensamiento critico
*Facilidad para la expresión de ideas
*Sentimiento de solidaridad
*Adecuada relación con las figuras de autoridad$

Desarrollo comunitario para el envejecimiento 7.50\
\
Características del estudiante\
*Tener herramientas básicas en el uso de tecnologías aplicadas a la educación
*Habilidad para integrar conocimientos precedentes con información novedosa
*Saber comunicar de manera oral y escrita tus ideas
*Aptitud para trabajar en equipo
*Paciencia para poder trabajar con personas en proceso de envejecimiento
*Poder realizar actividades en ambientes socioculturales diversos

SOCIOLOGIA\
El aspirante a estudiar la licenciatura en Sociología debe poseer inclinación por el análisis y la investigación sistemáticos, el trabajo en equipo y facilidad para la crítica constructiva que éste entraña.
Ante todo, interesarse por los problemas políticos, socioeconómicos y culturales de su sociedad, además de poseer algunas de las siguientes características: Ser imaginativo y disciplinado en el análisis de información periodística, documental y bibliográfica. Ser capaz de relacionarse fácilmente con individuos y grupos sociales, no sólo con quienes labora, sino además con los sujetos de estudio.
Asimismo, deberá reunir los siguientes requisitos académicos mínimos: Vocación para el estudio de teorías, doctrinas e ideologías sociales, así como por la historia social. Hábito por la lectura y el análisis de textos, así como capacidad para expresarse en forma oral y escrita.\\$
TRABAJO SOCIAL\
El aspirante debe contar con una actitud de servicio hacia la sociedad, sentido de solidaridad, poseer interés y facilidad para establecer adecuadas relaciones sociales. Tener hábito por la lectura y habilidad para revisar textos, aprendiendo, estructurando y sintetizando conceptos e ideas, interés en el trabajo de campo, ya que gran parte de las actividades que el trabajador social realiza con la población las lleva a cabo en dicho ámbito, por lo que es deseable tener habilidad para desplazarse y concurrir a sitios específicos donde se encuentre la población con la que trabajará.
Facilidad para comunicarse, de manera verbal y escrita, habilidad para hablar en público y buena redacción.\\$
 ECONOMIA INDUSTRIAL\
Para estudiar esta carrera deberás contar con las siguientes características: Inclinación y sensibilidad para conocer los problemas económicos, sociales y tecnológicos del país. Demostrar interés en el análisis científico. Conocimientos sólidos en las matemáticas.
En lo referente a las habilidades, es importante: Disposición y para el trabajo en equipo, así como para formar y dirigir grupos de trabajo. Flexibilidad para adaptarse a situaciones novedosas. Interés en la revisión de notas periodísticas relacionadas con la actividad económica, de finanzas, negocios y desarrollo tecnológico. Facilidad e interés para desenvolverse en el conocimiento de la ciencia económica y áreas relacionadas, como: la administración, las finanzas, los negocios y la ingeniería industrial.\\$
 ADMINISTRACION AGROPECUARIA\
Deberán poseer: habilidades para la búsqueda, análisis y síntesis de información; conocimientos básicos de matemáticas y estadística; capacidad para comunicarse de manera oral y escrita y establecer relaciones interpersonales; integrar y dirigir grupos de trabajo e interés en elevar los niveles de vida de los productores del campo y sus familias. En esta licenciatura se forman profesionales capaces de detectar, analizar, emprender y administrar proyectos agrícolas, pecuarios, piscícolas, forestales y medioambientales, aplicando los principios y técnicas administrativas, financieras, mercadológicas y de desarrollo de recursos humanos que beneficien a las comunidades rurales de nuestro país\\$
COMUNICACIÓN\
Además de haber cursado el bachillerato en el Área de las Ciencias Sociales es deseable que quien decida cursar la licenciatura en Comunicación en la FES Acatlán posea las siguientes características: Interés por el estudio y la búsqueda de soluciones a los problemas que la propia comunicación plantea, así como por el conocimiento social y humanístico. Conocimiento de la historia. Hábito de la lectura y habilidad de comprensión lectora. Capacidad crítica, reflexiva, de análisis y síntesis, dialógica, así como para el trabajo autónomo y grupal. Compromiso con la sociedad. Manejo de expresión oral y escrita.\\$
COMUNICACION Y PERIODISMO\
Requieres de facilidad para el manejo, la comparación y el análisis de los sucesos.
Asimismo, es indispensable que poseas un conocimiento óptimo de la gramática, disponibilidad para la lectura, dominio de la redacción, capacidad en el uso del lenguaje verbal y escrito, ya que el correcto uso del idioma será un instrumento básico de trabajo; además de mostrar interés por la información y la cultura de los medios impresos y audiovisuales, convencionales y de vanguardia.
Es importante poseer la facilidad de trato personal y la habilidad para trabajar en equipo, así como la imaginación, la creatividad y la inventiva, mismas que habrán de caracterizar al futuro profesional de la comunicación.
Las asignaturas relacionadas con investigación, redacción y Ciencias Sociales en general son las que de preferencia deberá haber cursado. Además, es recomendable haber acreditado el bachillerato en el Área de las Ciencias Sociales, o cursado asignaturas afines a ésta.\\$
 ESTUDIOS SOCIALES Y GESTION LOCAL\
Preferentemente, debe ser egresado de las áreas de Ciencias Sociales, de las Humanidades o de las Artes de la Escuela Nacional Preparatoria. En el caso de que sea egresado del Colegio de Ciencias y Humanidades o de otros programas de Educación Media Superior, deberá haber llevado el conjunto de asignaturas relacionadas con estos campos de conocimiento. Necesita de nociones básicas sobre los problemas sociales contemporáneos, además de habilidades para la comunicación oral y escrita, así como para el análisis y síntesis de información vinculada a lo social. A su vez, deberá mostrar interés en involucrarse activamente en la solución de problemas que afectan el bienestar y la calidad de vida de la sociedad mexicana, así como contar con la actitud para formarse en el manejo de métodos de investigación y análisis de problemáticas sociales emergentes, en el aprendizaje de técnicas de trabajo colaborativo y en la gestión de acciones y proyectos participativos.\\$
 DESARROLLO COMUNITARIO PARA EL ENVEJECIMIENTO\
Es necesario ser egresado de la Escuela Nacional Preparatoria, del Colegio de Ciencias y Humanidades o de otros programas de educación media superior y haber cursado alguna de las siguientes áreas en el bachillerato: Ciencias Sociales, Ciencias Biológicas y de la Salud, Ciencias Experimentales o Histórico-Social.
Específicamente el aspirante requiere de los siguientes conocimientos: Herramientas básicas sobre el uso de tecnologías aplicadas a la educación. La concepción del ser humano como una totalidad bio-psico-social.\
Habilidades para:\ *Integrar conocimientos precedentes con información novedosa. Comunicar con claridad, de forma oral y escrita, sus propias ideas con fundamentos teóricos. Construir y abstraer el conocimiento.
*Trabajar en equipo. *Trabajar con personas en proceso de envejecimiento. *Realizar actividades en ambientes socioculturales diversos.$
 ANTROPOLOGIA\
El estudiante interesado en ingresar a Antropología debe ser egresado de la Escuela Nacional Preparatoria, del Colegio de Ciencias y Humanidades, o de otros programas de Educación Media Superior.\
Es conveniente que haya cursado el área de las Ciencias Sociales o de las Humanidades y las Artes, o el área de conocimiento Histórico-Social. Para todos los casos, el perfil deseable incluye los siguientes conocimientos, habilidades y actitudes:\
*Conocimiento:  De los elementos teóricos y metodológicos en Ciencias Sociales. De los componentes básicos para comprender e interpretar la realidad social. Generales de geografía e historia de México y del mundo.
*Habilidades: Para comprender las problemáticas vinculadas a procesos sociales y culturales. Para relacionarse en contextos multiculturales.
*Actitudes: Interés por explorar fenómenos sociales y culturales presentes y pasados. Respeto por la diversidad cultural, étnica y social del país y el mundo. Disciplina en el estudio y gusto por la lectura.$
DESARROLLO TERRITORIAL\
El estudiante interesado en ingresar a Desarrollo Territorial debe ser egresado de la Escuela Nacional Preparatoria, del Colegio de Ciencias y Humanidades o de otros programas de Educación Media Superior. Es conveniente que haya cursado las áreas de las Ciencias Sociales, Humanidades y de las Artes, o área de conocimiento Histórico-Social y que posea conocimientos básicos de Economía, Geografía, Sociología, Matemáticas, y de las tecnologías de la información.\
\
*Habilidades:  De pensamiento lógico y crítico para analizar la realidad social. Capacidad de diálogo, análisis, reflexión crítica y síntesis. En lectura, escritura y redacción del español.
*Actitudes: Interés científico para analizar los problemas sociales, políticos y ambientales de los ámbitos locales, nacionales e internacionales. Compromiso social. Interés científico para analizar problemas sociales, económicos, políticos y ambientales nacionales e internacionales.$
NEGOCIOS INTERNACIONALES\
\
Es recomendable revisar los siguientes conocimientos, habilidades y actitudes deseables en el aspirante.\
*Conocimientos Básicos de: administración, economía, relaciones internacionales, derecho y campos disciplinarios afines. Generales de matemáticas y lógica. Dominio de las técnicas de investigación documental.
*Habilidades para: La lectura, escritura y redacción de textos en idioma español. Establecer y mantener relaciones personales que le permitan concertar acuerdos.
*Actitudes: Interés por el área de negocios y la gestión de empresas nacionales e internacionales. Creatividad para generar ideas relacionadas con el quehacer empresarial.
